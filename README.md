# admin

> Admin

## Development

### Install dependencies

    $ yarn

### Run app in development mode

    $ bin/poi dev

## Production

    $ bin/poi build
