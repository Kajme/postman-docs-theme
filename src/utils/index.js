const load = (component) => () => System.import(`@/components/${component}.vue`)

export default load
