import Vue from 'vue'
import App from './App'
import store from './store/'
import VueMaterial from 'vue-material'

Vue.use(VueMaterial)

const Root = Vue.component('app', App)

new Root({ // eslint-disable-line no-new
  el: '#app',
  store
})
