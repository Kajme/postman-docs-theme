import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
let docs = require('./postman.json')
const state = {
  // wallets: [{}]
  docs
}
export default new Vuex.Store({
  state
})
